using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Cache
{
    public interface ICacheService
    {
        Task<List<(string, string)>> GetStringListAsync(IEnumerable<string> keys);
        
        Task UpdateCacheAsync(IEnumerable<(string, string)> data);
    }
}